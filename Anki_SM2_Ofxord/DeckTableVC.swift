//
//  DeckTableVC.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 10.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import CoreData

public enum DecksMode: Int {
    case dictionary = 0
    case anki = 1
}

class DeckTableVC: UITableViewController {
    
// MARK: - Properties
    var coreDataStack: CoreDataStack = ((UIApplication.shared.delegate as? AppDelegate)?.coreDataStack)!
    var fetchedResultsController: NSFetchedResultsController<Deck>!
    
    var rightAddButton : UIBarButtonItem!
    
    public var mode = DecksMode.dictionary {
        didSet {
            if mode == .dictionary {
                self.navigationItem.rightBarButtonItem = self.rightAddButton
            } else {
                self.navigationItem.rightBarButtonItem = nil
            }
            tableView.reloadData()
        }
    }


    
// MARK: - IBActions
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        mode = DecksMode(rawValue: sender.selectedSegmentIndex)!
    }
    
    
    @IBAction func addDeckButton(_ sender: Any) {
        let alert = UIAlertController(title: "Title", message: "Type deck name", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
            guard let text = alert.textFields?.first?.text, !text.isEmpty, let context = self?.coreDataStack.managedContext else {
                alert.dismiss(animated: true, completion: nil)
                return
            }
            let deck = Deck(context: context)
            deck.releaseDate = NSDate()
            deck.name = text
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save Deck while adding it: \(error), \(error.userInfo)")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { action in
            alert.dismiss(animated: true, completion: nil)
            return
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
// MARK: - View Controller LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rightAddButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addDeckButton(_:)))
        self.navigationItem.rightBarButtonItem = self.rightAddButton
        
        let fetchRequest: NSFetchRequest<Deck> = Deck.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Deck.releaseDate), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: coreDataStack.managedContext,
                                                              sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController.delegate = self
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Could fetch the deck data in DeckTableVC: \(error), \(error.userInfo)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      
        if let identifier = segue.identifier {
            switch identifier {
            case "To Remembering Cards":
                if let destination = (segue.destination as? UINavigationController)?.visibleViewController as? QuizViewController,
                    let deck = (sender as? DeckAnkiViewCell)?.deck, let indexPath = fetchedResultsController.indexPath(forObject: deck) {
                    destination.deck = fetchedResultsController.object(at: indexPath)
                }
            case "Show User Cards":
                if let destination  = (segue.destination as? UINavigationController)?.visibleViewController as? CardListVC,
                    let sender = sender as? DeckCell, let indexPath = fetchedResultsController.indexPath(forObject: sender.deck) {
                    destination.coreDataStack = coreDataStack
                    destination.deck = fetchedResultsController.object(at: indexPath)
                }
            default:
                break
            }
        }
    }
}

// MARK: -  UITableViewDataSource

extension DeckTableVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionInfo = fetchedResultsController.sections?[section] else {
            return 0
        }
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        if mode == .dictionary {
            cell = tableView.dequeueReusableCell(withIdentifier: "DeckCell", for: indexPath)
            if let deckCell = cell as? DeckCell {
                deckCell.deck = fetchedResultsController.object(at: indexPath)
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "Deck Anki Cell", for: indexPath)
            if let deckAnkiCell = cell as? DeckAnkiViewCell {
                deckAnkiCell.deck = fetchedResultsController.object(at: indexPath)
            }
        }
        return cell
    }
    
}

// MARK: - NSFetchedResultsControllerDelegate
extension DeckTableVC: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? DeckCell {
                cell.deck = fetchedResultsController.object(at: indexPath)
            }
        }
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let indexSet = IndexSet(integer: sectionIndex)
        
        switch type {
        case .insert:
            tableView.insertSections(indexSet, with: .automatic)
        case .delete:
            tableView.deleteSections(indexSet, with: .automatic)
        default: break
        }
    }

}










