//
//  DeckAnkiViewCell.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 10.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import CoreData

class DeckAnkiViewCell: UITableViewCell {

    var deck: Deck! {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var oldCards: UILabel!
    @IBOutlet weak var refreshedCards: UILabel!
    @IBOutlet weak var newCards: UILabel!
    @IBOutlet weak var name: UILabel!
    
    private func updateUI() {
        name.text = deck.name
        refreshedCards.text = "\(deck.numberOfRefreshedCards())"
        newCards.text = "\(deck.numberOfNewCards())"
        oldCards.text = "\(deck.numberOfOldCards())"
    }

}







