//
//  DeckCell.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 10.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import CoreData

class DeckCell: UITableViewCell {
    
    var deck: Deck!{
        didSet {
            updateUI()
        }
    }
    var coreDataStack: CoreDataStack!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    fileprivate func updateUI() {
        nameLabel.text = deck.name
        quantityLabel.text = "\(deck.userCards?.count ?? 0)"
    }
    

}
