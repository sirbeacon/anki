//
//  CardRequest.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 02.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import UIKit

struct UserCardStruct: CustomStringConvertible {
    var original: String
    var translation: String
    var definitions: [String]?
    var examaples: [String]?
    var image: NSData?
    var synonyms: [String]?
    var pronunciation: NSData?
    var phrases: [String]?
    
    
    init(originalWord: String, translatedWord: String) {
        self.original = originalWord
        translation = translatedWord
    }
    
    init(userCard: UserCard) { // deprecated
        original = userCard.original ?? "none"
        translation = userCard.translation ?? "none"
        /*if let definitions = userCard.definitions?.allObjects as? [Definitions], definitions.count != 0 {
            self.definitions = definitions.flatMap { $0.definition }
        }
        if let examples = userCard.examples?.allObjects as? [Examaples], examples.count != 0 {
            examaples = examples.flatMap { $0.examaple }
        }*/
    }
    
    var description: String {
        var result = "Original Word: \(original), translation: \(translation)\n"
        
        result.append(definitions?.joined(separator: " ") ?? "definitions are nil\n")
        result.append(examaples?.joined(separator: " ") ?? "examples are nil\n")
        result.append(synonyms?.joined(separator: " ") ?? "synonyms are nil\n")
        result.append(image != nil ? "Image is not nil\n" : "Image is nil\n")
        result.append(pronunciation != nil ? "pronunciation is not nil" : "pronunciation is nil")
        
        return result
    }
}

