//
//  CardDescriptionVC.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 09.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import AVFoundation

class CardDescriptionVC: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var originalLabel: UILabel!
    @IBOutlet weak var translationLabel: UILabel!
    @IBOutlet weak var synonymsLabel: UILabel!
    @IBOutlet weak var examplesLabel: UILabel!
    @IBOutlet weak var definitionsLabel: UILabel!
    @IBOutlet weak var phrasesLabel: UILabel!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var refreshButtomOutlet: UIBarButtonItem!
    
    
    var activityIndicatorView = UIActivityIndicatorView()
    
    var request = Request()
    
    var isFetching: Bool = false {
        willSet {
            if !isFetching && newValue == true {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                activityIndicatorView.startAnimating()
                activityIndicatorView.isHidden = false
                refreshButtomOutlet?.isEnabled = false
            } else if isFetching && newValue == false {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                activityIndicatorView.stopAnimating()
                activityIndicatorView.isHidden = true
                refreshButtomOutlet?.isEnabled = true
            }
        }
    }
    
    @IBAction func refreshButton(_ sender: Any) {
        
        isFetching = true
        
        if let original = userCard?.original, original != "none" {
            request.makeRequestWith(word: original)
        }
    }
    
    var player: AVAudioPlayer!
    
    @IBAction func soundButton(_ sender: Any) {
        self.player?.stop()
        guard let soundData = userCard?.pronunciation, let pl = try? AVAudioPlayer(data: soundData as Data) else { return }
        self.player = pl
        self.player.prepareToPlay()
        self.player.play()
    }
    
    var userCard: UserCard? {
        didSet {
            updateUI()
        }
    }
    
    fileprivate func updateUI() {
        if let userCard = userCard {
            originalLabel?.text = userCard.original
            translationLabel?.text = userCard.translation
            examplesLabel?.text = userCard.arrayOfStringExamples?.joined(separator: ", ")
            definitionsLabel?.text = userCard.arrayOfStringDefinitions?.joined(separator: ", ")
            phrasesLabel?.text = userCard.arrayOfStringPhrases?.joined(separator: ", ")
            synonymsLabel?.text = userCard.arrayOfStringSynonyms?.joined(separator: ", ")
            if let imageData = userCard.image {
                imageView?.image = UIImage(data: imageData as Data)
            }
            mainStackView?.setNeedsDisplay() // redrawing the main stack
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request.delegate = self
        
        activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.activityIndicatorViewStyle = .whiteLarge
        activityIndicatorView.frame.origin = CGPoint(x: imageView.bounds.width / 2,
                                                 y: imageView.bounds.height / 2)
        imageView.addSubview(activityIndicatorView)
        activityIndicatorView.isHidden = true
        updateUI()
    }
    
}

// MARK: - RequestDelegate
extension CardDescriptionVC: RequestDelegate {
    
    func didFinishLoad(userCardStruct: UserCardStruct) {
        isFetching = false
        guard let managedContext = userCard?.managedObjectContext  else { return }
        userCard?.update(with: userCardStruct, context: managedContext)
        updateUI()
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save while refreshing a userCard in CardDescriptionVC: \(error), \(error.userInfo)")
        }
        isFetching = false
    }
    
    func didFinishWith(error: RequestError?) {
        isFetching = false
        guard let error = error else { return }
        let alert = UIAlertController(title: "Some errrors", message: error.description, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        let againButton = UIAlertAction(title: "Again", style: .default) { (action) in
            self.request.makeRequestWith(word: error.requestWord)
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(againButton)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    func didStartLoad() {
        isFetching = true
    }
}

