//
//  QuizViewController.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 10.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import CoreData



class QuizViewController: UIViewController {

// MARK: - Properties
    var deck: Deck!

    //var heightConstraint: NSLayoutConstraint!
    
    var currentUserCard: UserCard!
    
    var isThinking = true {
        willSet {/*
            if newValue == false && isThinking {
                //answerView.isHidden = false
                NSLayoutConstraint.activate([heightConstraint])
                setOfAnswers.isHidden = false
                showAnswerButton.isHidden = true
            } else if newValue == true && !isThinking {
                NSLayoutConstraint.deactivate([heightConstraint])
                setOfAnswers.isHidden = true
                showAnswerButton.isHidden = false
            } else if newValue == true && isThinking {
                NSLayoutConstraint.deactivate([heightConstraint])
                setOfAnswers.isHidden = true
                showAnswerButton.isHidden = false
            }*/
        }
    }
    
    
// MARK: - IBOutlets
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var answerView: UIStackView!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var synonymsLabel: UILabel!
    @IBOutlet weak var examplesLabel: UILabel!
    @IBOutlet weak var definitionsLabel: UILabel!
    @IBOutlet weak var phrasesLabel: UILabel!
    @IBOutlet weak var setOfAnswers: UIStackView!
    @IBOutlet weak var showAnswerButton: UIButton!
    
    @IBOutlet weak var soonLabel: UILabel!
    @IBOutlet weak var hardLabel: UILabel!
    @IBOutlet weak var goodLabel: UILabel!
    @IBOutlet weak var easyButton: UILabel!

// MARK: - IBActions
    @IBAction func showAnswer(_ sender: Any) {
        isThinking = false
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true) { [weak self]  in
            self?.deck.saveContext()
        }
    }
    
    @IBAction func answer(_ sender: UIButton) {
        isThinking = true
        if let buttonText = sender.titleLabel?.text {
            switch buttonText {
            case "Easy":
                currentUserCard.set(responseQuality: ResponseQuility.easy)
            case "Hard":
                currentUserCard.set(responseQuality: ResponseQuility.hard)
            case "Good":
                currentUserCard.set(responseQuality: ResponseQuility.good)
            case "Soon":
                currentUserCard.set(responseQuality: ResponseQuility.soon)
            default:
                break
            }
        }
        updateUI()
        deck.saveContext()
    }

    private func updateUI() {
        if let randUserCard = deck.getRandomUserCard() {
            currentUserCard = randUserCard
            
            questionLabel.text = randUserCard.original
            answerLabel.text = randUserCard.translation
            synonymsLabel.text = randUserCard.arrayOfStringSynonyms?.joined(separator: ", ")
            examplesLabel.text = randUserCard.arrayOfStringExamples?.joined(separator: ", ")
            definitionsLabel.text = randUserCard.arrayOfStringDefinitions?.joined(separator: ", ")
            phrasesLabel.text = randUserCard.arrayOfStringPhrases?.joined(separator: ", ")
            
            soonLabel.text = "<10m"
            hardLabel.text = "\(Int(randUserCard.nextInterval(for: .hard)))"
            goodLabel.text = "\(Int(randUserCard.nextInterval(for: .good)))"
            easyButton.text = "\(Int(randUserCard.nextInterval(for: .easy)))"
            
            isThinking = true
        } else {
            showEndedView()
        }
    }
    
    private func showEndedView() {
        let endedView = UIView(frame: self.view.frame)
        endedView.backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: endedView.bounds.width / 2,
                                          y: endedView.bounds.height / 2,
                                          width: 0, height: 0))
        label.text = "End!"
        label.sizeToFit()
        self.view.addSubview(endedView)
        endedView.addSubview(label)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //heightConstraint = answerView.heightAnchor.constraint(equalToConstant: 0.0)
        //NSLayoutConstraint.activate([heightConstraint])
        if let userCard = deck.getRandomUserCard() {
            currentUserCard = userCard
        } else {
            showEndedView()
        }
        isThinking = true
        updateUI()
    }
    
}
