//
//  ContainerVC.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 12.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit

class ContainerVC: UIViewController {

    var deckTableVC: DeckTableVC?
    
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        deckTableVC?.mode = DecksMode(rawValue: sender.selectedSegmentIndex)!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embed segue", let destination = (segue.destination as? UINavigationController)?.visibleViewController as? DeckTableVC {
            self.deckTableVC = destination
        }
    }

}
