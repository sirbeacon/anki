//
//  UserCard+CoreDataProperties.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 10.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import CoreData


extension UserCard {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserCard> {
        return NSFetchRequest<UserCard>(entityName: "UserCard")
    }

    @NSManaged public var commitDate: NSDate?
    @NSManaged public var easinessFactor: Double
    @NSManaged public var image: NSData?
    @NSManaged public var interval: Double
    @NSManaged public var nextDateRepetition: NSDate?
    @NSManaged public var original: String?
    @NSManaged public var pronunciation: NSData?
    @NSManaged public var refreshed: Bool
    @NSManaged public var releaseDate: NSDate?
    @NSManaged public var repetitions: Int32
    @NSManaged public var translation: String?
    @NSManaged public var deck: Deck?
    @NSManaged public var definitions: NSSet?
    @NSManaged public var examples: NSSet?
    @NSManaged public var phrases: NSSet?
    @NSManaged public var synonyms: NSSet?

}

// MARK: Generated accessors for definitions
extension UserCard {

    @objc(addDefinitionsObject:)
    @NSManaged public func addToDefinitions(_ value: Definition)

    @objc(removeDefinitionsObject:)
    @NSManaged public func removeFromDefinitions(_ value: Definition)

    @objc(addDefinitions:)
    @NSManaged public func addToDefinitions(_ values: NSSet)

    @objc(removeDefinitions:)
    @NSManaged public func removeFromDefinitions(_ values: NSSet)

}

// MARK: Generated accessors for examples
extension UserCard {

    @objc(addExamplesObject:)
    @NSManaged public func addToExamples(_ value: Example)

    @objc(removeExamplesObject:)
    @NSManaged public func removeFromExamples(_ value: Example)

    @objc(addExamples:)
    @NSManaged public func addToExamples(_ values: NSSet)

    @objc(removeExamples:)
    @NSManaged public func removeFromExamples(_ values: NSSet)

}

// MARK: Generated accessors for phrases
extension UserCard {

    @objc(addPhrasesObject:)
    @NSManaged public func addToPhrases(_ value: Phrase)

    @objc(removePhrasesObject:)
    @NSManaged public func removeFromPhrases(_ value: Phrase)

    @objc(addPhrases:)
    @NSManaged public func addToPhrases(_ values: NSSet)

    @objc(removePhrases:)
    @NSManaged public func removeFromPhrases(_ values: NSSet)

}

// MARK: Generated accessors for synonyms
extension UserCard {

    @objc(addSynonymsObject:)
    @NSManaged public func addToSynonyms(_ value: Synonym)

    @objc(removeSynonymsObject:)
    @NSManaged public func removeFromSynonyms(_ value: Synonym)

    @objc(addSynonyms:)
    @NSManaged public func addToSynonyms(_ values: NSSet)

    @objc(removeSynonyms:)
    @NSManaged public func removeFromSynonyms(_ values: NSSet)

}
