//
//  ViewController.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 02.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import CoreData


class CardListVC: UITableViewController {
    
    var request = Request()
    
// MARK: - properties
    var coreDataStack: CoreDataStack!
    var fetchedResultsController: NSFetchedResultsController<UserCard>!
    var deck: Deck!
    
    
    
// MARK: - IBActions
    @IBAction func refresh(_ sender: UIRefreshControl) {
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Could not perform fetch while viewDidLoad method: \(error), \(error.userInfo)")
        }
        refreshControl?.endRefreshing()
    }

    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true) { [weak self] in
            self?.deck.saveContext()
        }
    }
    
///////// Ading a new card by Alert Controller
    @IBAction func addWord(_ sender: Any) {
        let alert = UIAlertController(title: "New word?", message: "Just type a word", preferredStyle: .alert)
        alert.addTextField(configurationHandler: nil)
        let okAction = UIAlertAction(title: "OK", style: .default) { [weak self] action in
            guard let word = alert.textFields?.first?.text, !word.isEmpty else {
                alert.dismiss(animated: true)
                return
            }
            self?.request.makeRequestWith(word: word)
            alert.dismiss(animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { action in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
// MARK: - ViewController LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        request.delegate = self
        
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "deck == %@", deck)
        let sort = NSSortDescriptor(key: #keyPath(UserCard.releaseDate), ascending: false)
        fetchRequest.sortDescriptors = [sort]
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                              managedObjectContext: coreDataStack.managedContext,
                                                              sectionNameKeyPath: nil, cacheName: nil)
        
        fetchedResultsController.delegate = self
        
        do {
            try fetchedResultsController.performFetch()
        } catch let error as NSError {
            print("Could not perform fetch while viewDidLoad method: \(error), \(error.userInfo)")
        }
    }
    
    deinit {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CardDescriptionVC,
            segue.identifier == "Show Full Card Description", let indexPath = sender as? IndexPath {
            let userCard = fetchedResultsController.object(at: indexPath)
            destination.userCard = userCard
            destination.title = userCard.original
        }
    }
    
}


//MARK: - UITable​View​Delegate 
extension CardListVC {
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        performSegue(withIdentifier: "Show Full Card Description", sender: indexPath)
    }
}

// MARK: - UITableViewDataSource
extension CardListVC {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionInfo = fetchedResultsController.sections?[section] else {
            return 0
        }
        return sectionInfo.numberOfObjects
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Card", for: indexPath)

        if let userCardCell = cell as? UserCardCell{
            userCardCell.userCard = fetchedResultsController.object(at: indexPath)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            coreDataStack.managedContext.delete(fetchedResultsController.object(at: indexPath))
            coreDataStack.saveContext()
        }
        
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension CardListVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .automatic)
            tableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? UserCardCell {
                cell.userCard = fetchedResultsController.object(at: indexPath)
            }
        }
    }


    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        let indexSet = IndexSet(integer: sectionIndex)
        
        switch type {
        case .insert:
            tableView.insertSections(indexSet, with: .automatic)
        case .delete:
            tableView.deleteSections(indexSet, with: .automatic)
        default: break
        }
    }

}

// MARK: - RequestDelegate
extension CardListVC: RequestDelegate {
    
    func didFinishLoad(userCardStruct: UserCardStruct) {
        /// Creating a UserCard form request
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        let userCard = UserCard(context: coreDataStack.managedContext)
        userCard.update(with: userCardStruct, context: coreDataStack.managedContext)
        userCard.releaseDate = NSDate()
        userCard.deck = deck

        coreDataStack.saveContext()
    }
    
    func didFinishWith(error: RequestError?) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        guard let error = error else { return }
        let alert = UIAlertController(title: "Some errrors", message: error.description, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        let againButton = UIAlertAction(title: "Again", style: .default) { (action) in
            self.request.makeRequestWith(word: error.requestWord)
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(againButton)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    func didStartLoad() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
}




















