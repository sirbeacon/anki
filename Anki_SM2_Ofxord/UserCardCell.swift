//
//  UserCardCell.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 04.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import UIKit
import AVFoundation

class UserCardCell: UITableViewCell {

    var player: AVAudioPlayer!
    
    @IBOutlet weak var originalLabel: UILabel!
    @IBOutlet weak var translationLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBAction func horn(_ sender: Any) {
        self.player?.stop()
        guard let soundData = userCard?.pronunciation, let pl = try? AVAudioPlayer(data: soundData as Data) else { return }
        self.player = pl
        self.player.prepareToPlay()
        self.player.play()
    }
    
    var userCard: UserCard? {
        didSet {
            if let userCard = userCard {
                originalLabel.text = userCard.original
                translationLabel.text = userCard.translation
                if let imageData = userCard.image {
                    userImageView.image = UIImage(data: imageData as Data)
                }
            }
        }
    }
   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }

}
