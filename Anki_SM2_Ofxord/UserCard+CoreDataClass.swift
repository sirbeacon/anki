//
//  UserCard+CoreDataClass.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 07.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import CoreData

public enum UserCardStatus {
    case new
    case old
    case refreshed
}

public enum ResponseQuility: Int {
    case soon = 0
    case hard = 2
    case good = 5
    case easy = 6
}

extension Int {
    static func roundToMathDouble(_ double: Double) -> Int{
        let i: Int = Int(double)
        if double - Double(i) >= 0.5 {
            return i + 1
        } else {
            return i
        }
    }
}

public class UserCard: NSManagedObject {

    public func getStatus() -> UserCardStatus {
        if refreshed == true {
            return .refreshed
        } else if commitDate == nil && refreshed == false && interval == 0 {
            return .new
        } else {
            return .old
        }
    }
    
    public func nextInterval(for responseQuality: ResponseQuility) -> Int {
        if getStatus() == .new || getStatus() == .refreshed {
            return intervalForNewCardsDependingOnEFactor[responseQuality]!
        } else {
            return Int.roundToMathDouble(interval * nextEasinessFactor(for: responseQuality))
        }
    }
    
    public func set(responseQuality: ResponseQuility) {
        if responseQuality == .soon {
            setRefreshed()
        } else {
            if self.getStatus() == .old {
                computedEFactor = easinessFactor + EFactor(responseQuality.rawValue)
                interval = interval * easinessFactor
                commitDate = NSDate()
                nextDateRepetition = commitDate?.addingTimeInterval(interval * 24 * 60 * 60)
            } else if self.getStatus() == .new {
                interval = 0
                easinessFactor = 1.3
                commitDate = NSDate()
                interval = Double(intervalForNewCardsDependingOnEFactor[responseQuality]!)
            } else if self.getStatus() == .refreshed {
                interval = Double(intervalForNewCardsDependingOnEFactor[responseQuality]!)
                computedEFactor = easinessFactor + EFactor(responseQuality.rawValue)
                commitDate = NSDate()
                nextDateRepetition = commitDate?.addingTimeInterval(interval * 24 * 60 * 60)
                refreshed = false
            }
        }
    }
    
    // MARK: private
    
    private func nextEasinessFactor(for response: ResponseQuility) -> Double {
        let result = easinessFactor + EFactor(response.rawValue)
        if result > 2.5 {
            return 2.5
        } else if result < 1.3 {
            return 1.3
        } else {
            return result
        }
    }
    
    private func setRefreshed() {
        interval = 0
        easinessFactor = 1.3
        commitDate = NSDate()
        refreshed = true
        nextDateRepetition = commitDate?.addingTimeInterval(10 * 60)
    }
    
    private let intervalForNewCardsDependingOnEFactor: [ResponseQuility: Int] = [
        .soon : 0,
        .hard : 1,
        .good : 2,
        .easy : 4
    ]
    
    private func saveContext() {
        if let context = self.managedObjectContext {
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save while updating quiz: \(error), \(error.userInfo)")
            }
        }
    }
    
    private func EFactor(_ value: Int) -> Double {
        let q = Double(value)
        return (0.1-(5-q)*(0.08+(5-q)*0.02))
    }
    
    private var computedEFactor: Double {
        set {
            if newValue >= 1.3 && newValue <= 2.5 {
                easinessFactor = newValue
            } else if newValue > 2.5 {
                easinessFactor = 2.5
            } else {
                easinessFactor = 1.3
            }
        }
        get {
            return easinessFactor
        }
    }
    
// MARK: - Quizing
    
    /*func set(eFactor: EFactor) {
        switch eFactor {
        case .good:
            employSuperMemo2(.good)
        case .hard:
            employSuperMemo2(.hard)
        case .easy:
            employSuperMemo2(.easy)
        case .soon:
            tappedRefreshed()
        }
    }
    
    
    
    
    
    var status: UserCardStatus {
        if commitDate == nil && interval == 0 { return .new }
        else if refreshed { return .refreshed }
        else { return .old }
    }
    
    func nextInterval(for ef: EFactor) -> Double {
        if (ef != .soon && interval == 0) || refreshed {
            return intervalFor(ef)
        } else {
            return interval * verifyEf(Double(ef.rawValue))
        }
    }
    
    private func verifyEf(_ value: Double) -> Double {
        if value >= 1.3 && value <= 2.5 {
            return value
        } else if value >= 2.5 {
            return 2.5
        } else {
             return 1.3
        }
    }
    
    private func tappedRefreshed() {
        refreshed = true
        computedEasinessFactor = 1.3
        interval = 0
        commitDate = NSDate()
        nextDateRepetition = commitDate?.addingTimeInterval(10 * 60)
    }
    
    private func saveContext() {
        if let context = self.managedObjectContext {
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save while updating quiz: \(error), \(error.userInfo)")
            }
        }
    }
    
    private func employSuperMemo2(_ ef: EFactor) {
        if !refreshed && commitDate != nil {
            let newEF = computedEasinessFactor + EF(ef.rawValue)
            let newInterval = interval * newEF
            commitDate = NSDate()
            nextDateRepetition = commitDate?.addingTimeInterval(newInterval * 24 * 60 * 60)
            computedEasinessFactor = newEF
            interval = newInterval
        } else {
            if ef != .soon {
                computedEasinessFactor = 1.3 + EF(ef.rawValue)
                interval = intervalFor(ef)
                commitDate = NSDate()
                nextDateRepetition = commitDate?.addingTimeInterval(interval * 24 * 60 * 60)
            } else {
                tappedRefreshed()
            }
        }
        saveContext()
    }
    
    private func intervalFor(_ ef: EFactor) -> Double {
        switch ef {
        case .easy:
            return 4
        case .good:
            return 2
        case .hard:
            return 1
        case .soon:
            return 0
        }
    }
    
    private func EF(_ value: Int) -> Double {
        let q = Double(value)
        return (0.1-(5-q)*(0.08+(5-q)*0.02))
    }
    
    var computedEasinessFactor: Double {
        set {
            if newValue >= 1.3 && newValue <= 2.5 {
                easinessFactor = newValue
            }
        } get {
            return easinessFactor
        }
    }*/
    
// MARK: - Converting Stuff
    func update(with userCardStruct: UserCardStruct, context: NSManagedObjectContext) {
        self.original = userCardStruct.original
        self.translation = userCardStruct.translation
        self.pronunciation = userCardStruct.pronunciation
        self.image = userCardStruct.image
        getDefinitions(from: userCardStruct.definitions, context: context)
        getExamples(from: userCardStruct.examaples, context: context)
        getPhrases(from: userCardStruct.phrases, context: context)
        getSynonyms(from: userCardStruct.synonyms, context: context)
    }
    
    private func getDefinitions(from strings: [String]?, context: NSManagedObjectContext) {
        if let previousDefinitions = definitions {
            removeFromDefinitions(previousDefinitions)
        }
        guard let strings = strings, !strings.isEmpty else { return }
        for def in strings {
            let definition = Definition(context: context)
            definition.stringValue = def
            self.addToDefinitions(definition)
        }
    }
    
    private func getExamples(from strings: [String]?, context: NSManagedObjectContext) {
        if let previousExamples = examples {
            removeFromExamples(previousExamples)
        }
        guard let strings = strings, !strings.isEmpty else { return }
        for ex in strings {
            let example = Example(context: context)
            example.stringValue = ex
            self.addToExamples(example)
        }
    }
    
    private func getPhrases(from strings: [String]?, context: NSManagedObjectContext) {
        if let previousPhrases = phrases {
            removeFromPhrases(previousPhrases)
        }
        guard let strings = strings, !strings.isEmpty else { return }
        for ph in strings {
            let phrase = Phrase(context: context)
            phrase.stringValue = ph
            self.addToPhrases(phrase)
        }
    }
    
    private func getSynonyms(from strings: [String]?, context: NSManagedObjectContext) {
        if let previousSynonyms = synonyms {
            removeFromSynonyms(previousSynonyms)
        }
        guard let strings = strings, !strings.isEmpty else { return }
        for syn in strings {
            let synonym = Synonym(context: context)
            synonym.stringValue = syn
            self.addToSynonyms(synonym)
        }
    }
    
    var arrayOfStringSynonyms: [String]? {
        guard let syn = synonyms?.allObjects as? [Synonym], !syn.isEmpty else {
            return nil
        }
        return syn.flatMap { $0.stringValue }
    }
    
    var arrayOfStringDefinitions: [String]? {
        guard let def = definitions?.allObjects as? [Definition], !def.isEmpty else {
            return nil
        }
        return def.flatMap { $0.stringValue }
    }
    
    var arrayOfStringExamples: [String]? {
        guard let ex = examples?.allObjects as? [Example], !ex.isEmpty else {
            return nil
        }
        return ex.flatMap { $0.stringValue }
    }
    
    var arrayOfStringPhrases: [String]? {
        guard let phr = phrases?.allObjects as? [Phrase], !phr.isEmpty else {
            return nil
        }
        return phr.flatMap { $0.stringValue }
    }
}







