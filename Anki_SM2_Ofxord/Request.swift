//
//  NewRequest.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 27.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import SwiftyJSON


protocol RequestDelegate: class {
    func didFinishWith(error: RequestError?)
    func didFinishLoad(userCardStruct: UserCardStruct)
    func didStartLoad()
}

extension RequestDelegate {
    func didFinishWith(error: RequestError?) {
        // optional because of struct
    }
    func didStartLoad() { }
}

enum RequestErrorKind: String {
    case wrongDictionarySet = "Phrases"
    case wrongTranslate = "Translate"
    case wrongSynonyms = "Synonyms"
    case wrongMainOxfordJSON = "Examples, definitions, pronunciation" // depends on main json oxford methond
    case wrongImage = "Image"
}

struct RequestError: Error, CustomStringConvertible {
    var errors: Set<RequestErrorKind> = Set()
    var requestWord: String
    
    var description: String {
        var result = "Could not fetch: "
        for error in errors {
            result = result + error.rawValue
        }
        return result
    }
}

class Request {
    
    private lazy var session: URLSession = {
        let config = URLSessionConfiguration.ephemeral
        return URLSession(configuration: config)
    }()
    
    weak var delegate: RequestDelegate?
    
    deinit {
        delegate?.didFinishWith(error: nil)
    }
    
    ////////////////////////////////////////////////////////
    // This is the main request method, here all begins ////
    ////////////////////////////////////////////////////////
    func makeRequestWith(word: String) {
        let word_id = "\(word.lowercased())"
        var requestError = RequestError(errors: Set(), requestWord: word_id)
        // first, getting the translation, if not push the error and do not create user card
        delegate?.didStartLoad()
        getTranslate(from: word_id) { [weak self] translatedWord, error in
            guard let translatedWord = translatedWord, error == nil else {
                requestError.errors.insert(RequestErrorKind.wrongTranslate)
                self?.delegate?.didFinishWith(error: requestError)
                return
            }
            var userCard = UserCardStruct(originalWord: word_id, translatedWord: translatedWord)
            let group = DispatchGroup()

            group.enter()
            self?.getDictionarySet(from: word_id, completion: { (json, reqError) in
                if reqError == nil, let json = json {
                    userCard.phrases = self?.getPhrases(from: json)
                } else if let reqError = reqError {
                    requestError.errors.insert(reqError)
                }
                group.leave()
            })
            
            group.enter()
            self?.getTheMainJSONfromOxfordAPI(from: word_id, completion: { (json, reqError) in
                if reqError == nil, let json = json {
                    userCard.definitions = self?.getDefinitons(from: json)
                    userCard.examaples = self?.getExamples(from: json)
                    group.enter()
                    self?.getPronunciation(form: json, completion: { (data, error) in
                        if let data = data, error == nil {
                            userCard.pronunciation = data
                        }
                        group.leave()
                    })
                } else if let reqError = reqError {
                    requestError.errors.insert(reqError)
                }
                group.leave()
            })
            
            group.enter()
            self?.getSynonyms(from: word_id, completion: { (synonyms, reqError) in
                if let synonyms = synonyms, reqError == nil {
                    userCard.synonyms = synonyms
                } else if let reqError = reqError {
                    requestError.errors.insert(reqError)
                }
                group.leave()
            })
            
            group.enter()
            self?.getRandomImage(from: word_id, completion: { (data, reqError) in
                if let data = data, reqError == nil {
                    userCard.image = data
                } else if let reqError = reqError {
                    requestError.errors.insert(reqError)
                }
                group.leave()
            })
            
            group.notify(queue: DispatchQueue.main) {
                if !requestError.errors.isEmpty {
                    self?.delegate?.didFinishWith(error: requestError)
                }
                self?.delegate?.didFinishLoad(userCardStruct: userCard)
            }
        }
    }
    
    
/////////////////
// Yandex API ///
/////////////////
    fileprivate static let translaterKey = "trnsl.1.1.20170403T132030Z.b635ae907b1579f5.f0e4120408171ba333e3826b9890aa587a20634d"
    fileprivate static let dictionaryKey = "dict.1.1.20170403T134005Z.9a3f7beaba6d5f4e.c52960138af291704b31bfd01e79dc5cb1ed1e11"
    fileprivate static let urlStringForTranslate = "https://translate.yandex.net/api/v1.5/tr.json/translate"
    fileprivate static let urlStringForDictionary = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup"
    
    private func getUrl(from dict: [String:String], and mainUrl: String) -> URL? {
        var queryItems: [URLQueryItem] = []
        for (key, value) in dict {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        if var components = URLComponents(string: mainUrl) {
            components.queryItems = queryItems
            return components.url
        }
        return nil
    }
    
   
    private func getDictionarySet(from word: String, completion: @escaping (JSON?, RequestErrorKind?) -> Void) {
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        let parameters = ["key" : Request.dictionaryKey,
                          "lang" : "en-ru",
                          "text" : word]
        request.url = getUrl(from: parameters, and: Request.urlStringForDictionary)
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                completion(JSON(data), nil)
            } else {
                print("getDictionarySet() method error \(error?.localizedDescription ?? "error??"):")
                completion(nil, RequestErrorKind.wrongDictionarySet)
            }
        }
        task.resume()
    }
    
    func getTranslate(from word: String, completion: @escaping (String?, RequestErrorKind?) -> Void) {
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        let parameters = [ "key" : Request.translaterKey,
                           "lang" : "en-ru",
                           "text" : word]
        request.url = getUrl(from: parameters, and: Request.urlStringForTranslate)
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                completion(JSON(data)["text"][0].string ?? "🤷‍♂️nope", nil)
            } else {
                print("getTranslate() methond error: \(error?.localizedDescription ?? "error??")")
                completion(nil, RequestErrorKind.wrongTranslate)
            }
        }
        task.resume()
    }
    
    func getPhrases(from json: JSON?) -> [String]? {
        guard let json = json else { return nil }
        let temp = json["def"].arrayValue.map({ $0["tr"].arrayValue.map { $0["ex"].arrayValue.map { $0["text"].stringValue } } })
        let res = temp.flatMap { $0 }.flatMap { $0 } /// HELP ME THIS CODE SNIPPET IS BULLSHIT!!!
        return !res.isEmpty ? res : nil
    }
    
    
/////////////////////
// Oxford API ////////
// constants and keys for the API requests
////////////////////////
    fileprivate static let APIBaseURL = "https://od-api.oxforddictionaries.com/api/v1"  // Consistent part of API requests.
    fileprivate static let applicationID = "01bd7144" // This is the application ID, you should send with each API request.
    fileprivate static let applicationKey = "213e2f8b8c16e3a66fe1b84d1c71a0e3" // to authenticate requests.
    fileprivate static let language = "en"
    
    fileprivate static var urlString: String {
        return "https://od-api.oxforddictionaries.com:443/api/v1/entries/\(language)/"
    }
    fileprivate static var headers: [String : String] {
        return [
            "Accept" : "application/json",
            "app_id" : applicationID,
            "app_key" : applicationKey
        ]
    }
    
    func getSynonyms(from word: String, completion: @escaping ([String]?, RequestErrorKind?) -> Void) {
        let url = URL(string: Request.urlString + word.lowercased() + "/synonyms")!
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(Request.applicationID, forHTTPHeaderField: "app_id")
        request.addValue(Request.applicationKey, forHTTPHeaderField: "app_key")
        request.url = url
        let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
            if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                let json = JSON(data)["results"][0]["lexicalEntries"][0]["entries"][0]["senses"].arrayValue.map { $0["synonyms"] }
                let result = json[0].arrayValue.map { $0["text"].stringValue }
                completion(!result.isEmpty ? result : nil, nil)
            } else {
                print("getSynonyms method error: \(error?.localizedDescription ?? "error??")")
                completion(nil, RequestErrorKind.wrongSynonyms)
            }
        })
        task.resume()
    }
    
    private func getTheMainJSONfromOxfordAPI(from word: String, completion: @escaping (JSON?, RequestErrorKind?) -> Void) {
        let url = URL(string: Request.urlString + word.lowercased())!
        let request = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(Request.applicationID, forHTTPHeaderField: "app_id")
        request.addValue(Request.applicationKey, forHTTPHeaderField: "app_key")
        request.url = url
        let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
            if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                completion(JSON(data), nil)
            } else {
                print("getTheMainJSONfromOxfordAPI method error: \(error?.localizedDescription ?? "error??")")
                completion(nil, RequestErrorKind.wrongMainOxfordJSON)
            }
        }
        task.resume()
    }
    
    // getting the optional array! maybe should return just empty array I'm not so sure
    fileprivate func getDefinitons(from JSON: JSON?) -> [String]? {
        guard let JSON = JSON else { return nil }
        let entries = JSON["results"][0]["lexicalEntries"].arrayValue.map({ $0["entries"] })
        let senses = entries.map { $0[0]["senses"] }
        let result = senses.map({ $0[0]["definitions"] }).map { $0[0].string }.flatMap { $0 }
        return !result.isEmpty ? result : nil
    }
    
    // just like previous method
    fileprivate func getExamples(from JSON: JSON?) -> [String]? {
        guard let JSON = JSON else { return nil }
        let entries = JSON["results"][0]["lexicalEntries"].arrayValue.map({ $0["entries"] })
        let senses = entries.map { $0[0]["senses"] }
        let examples = senses.map { $0[0]["examples"][0]["text"].string }.flatMap { $0 }
        return !examples.isEmpty ? examples : nil
    }
    /// Getting Pronunciation
    fileprivate func getPronunciation(form JSON: JSON?,completion: @escaping (NSData?,Error?) -> Void) {
        guard let JSON = JSON else { return }
        let soundPath = JSON["results"][0]["lexicalEntries"][0]["pronunciations"][0]["audioFile"].string
        DispatchQueue.global(qos: .userInitiated).async {
            guard let soundPath = soundPath, let soundURL = URL(string: soundPath), let soundData = NSData(contentsOf: soundURL) else {
                completion(nil, nil)
                return
            }
            completion(soundData, nil)
        }
    }
    
    
    
    ////////////////////////
    //// Flickr Search /////
    ////////////////////////
    
    static let apiFlickrKey = "e8e21ff09d8858cb39b1c1db081a2bc1"
    
    func getRandomImage(from word: String, completion: @escaping (NSData?, RequestErrorKind?) -> Void) {
        guard let escapedTerm = word.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else { return }
        print(escapedTerm)
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(Request.apiFlickrKey)&text=\(escapedTerm)&format=json&nojsoncallback=1"
        guard let url = URL(string:URLString) else { return }
        let task = session.dataTask(with: url) { (data, response, error) in
            if error == nil, let data = data, (response as? HTTPURLResponse)?.statusCode == 200 {
                let json = JSON(data)
                let arrayImages = json["photos"]["photo"].arrayValue
                let rand = Int(arc4random_uniform(UInt32(arrayImages.count)))
                let randImage = arrayImages[rand]
                let serverId = randImage["server"].stringValue
                let id = randImage["id"].stringValue
                let secret = randImage["secret"].stringValue
                let url = URL(string: "https://farm1.staticflickr.com/\(serverId)/\(id)_\(secret)_q.jpg")!
                DispatchQueue.global(qos: .userInitiated).async {
                    if let data = NSData(contentsOf: url) {
                        completion(data, nil)
                    } else {
                        completion(nil, RequestErrorKind.wrongImage)
                    }
                }
            }
        }
        task.resume()
    }
    
    
}


















