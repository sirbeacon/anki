//
//  Deck+CoreDataClass.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 12.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import CoreData

@objc(Deck)
public class Deck: NSManagedObject {
    
//MARK: Private variables
    private var oldCardsPredicateFormat = "interval > 0 AND refreshed == FALSE AND commitDate != NULL AND deck == %@ AND nextDateRepetition < %@"
    private var newCardsPredicateFormat = "commitDate == NULL AND interval == 0 AND refreshed == FALSE AND deck == %@"
    private var refreshedPredicateFormat = "refreshed == TRUE AND deck == %@ AND nextDateRepetition < %@"
    
    
    public func saveContext() {
        if let context = self.managedObjectContext {
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save in saveContext() of deck: \(error), \(error.userInfo)")
            }
        } else {
            print("Could not get the context in saveContext() of deck")
        }
    }
    
// MARK: getting cards
    
    public func getRandomUserCard() -> UserCard? {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "nextDateRepetition < %@ OR commitDate == NULL", NSDate())
        if let context = self.managedObjectContext {
            do {
                let result = try context.fetch(fetchRequest)
                if result.count != 0 {
                    let rand: Int = Int(arc4random_uniform(UInt32(result.count)))
                    return result[rand]
                }
            } catch let error as NSError {
                print("Could not getRandomUserCard(): \(error), \(error.userInfo)")
            }
        }
        return nil
    }
    
    public func getRefreshedCards() -> [UserCard] {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: refreshedPredicateFormat, self, NSDate())
        if let context = self.managedObjectContext {
            do {
                return try context.fetch(fetchRequest)
            } catch let error as NSError {
                print("Could not fetch refreshed cards in numberOfRefreshedCards(): \(error), \(error.userInfo)")
            }
        }
        return [UserCard]()
    }
    
    public func getOldCards() -> [UserCard] {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: oldCardsPredicateFormat, self, NSDate())
        if let context = self.managedObjectContext {
            do {
                return try context.fetch(fetchRequest)
            } catch let error as NSError {
                print("Could not get old cards in numberOfOldCards(): \(error), \(error.userInfo)")
            }
        }
        return [UserCard]()

    }
    
    public func getNewCards() -> [UserCard] {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: newCardsPredicateFormat, self)
        if let context = self.managedObjectContext {
            do {
                return try context.fetch(fetchRequest)
            } catch let error as NSError {
                print("Could not fetch user cards in getNewCards(): \(error), \(error)")
            }
        }
        return [UserCard]()
    }
    
    
// MARK getting numbers of cards
    
    public func  numberOfRefreshedCards() -> Int {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: refreshedPredicateFormat, self, NSDate())
        if let context = self.managedObjectContext {
            do {
                return try context.count(for: fetchRequest)
            } catch let error as NSError {
                print("Could not fetch refreshed cards in numberOfRefreshedCards(): \(error), \(error.userInfo)")
            }
        }
        return 0
    }
    
    public func numberOfOldCards() -> Int {
        let fetchRequest: NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: oldCardsPredicateFormat, self, NSDate())
        if let context = self.managedObjectContext {
            do {
                return try context.count(for: fetchRequest)
            } catch let error as NSError {
                print("Could not get old cards in numberOfOldCards(): \(error), \(error.userInfo)")
            }
        }
        return 0
    }
    
    public func numberOfNewCards() -> Int {
        let fetchRequest:NSFetchRequest<UserCard> = UserCard.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: newCardsPredicateFormat, self)
        if let context = self.managedObjectContext {
            do {
                return try context.count(for: fetchRequest)
            } catch let error as NSError {
                print("Could not fetch user cards in numberOfNewCards(): \(error), \(error)")
            }
        }
        return 0
    }
    
}
