//
//  Deck+CoreDataProperties.swift
//  Anki_SM2_Ofxord
//
//  Created by Никита Прохоров on 12.04.17.
//  Copyright © 2017 N_P. All rights reserved.
//

import Foundation
import CoreData


extension Deck {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Deck> {
        return NSFetchRequest<Deck>(entityName: "Deck")
    }

    @NSManaged public var name: String?
    @NSManaged public var releaseDate: NSDate?
    @NSManaged public var userCards: NSSet?

}

// MARK: Generated accessors for userCards
extension Deck {

    @objc(addUserCardsObject:)
    @NSManaged public func addToUserCards(_ value: UserCard)

    @objc(removeUserCardsObject:)
    @NSManaged public func removeFromUserCards(_ value: UserCard)

    @objc(addUserCards:)
    @NSManaged public func addToUserCards(_ values: NSSet)

    @objc(removeUserCards:)
    @NSManaged public func removeFromUserCards(_ values: NSSet)

}
